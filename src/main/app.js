const request = require('request');
const { Base64Binary } = require('../functions/Base64Decoder');
const serviceConfig = require('../config/ServiceConfig');
const signatureConfig = require('../config/SignatureConfig');
const { cipher, getPublicKey, setPublicAndPrivateKey } = require('../functions/Cipher');

var fs = require('fs');

this.headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${serviceConfig.ACCESS_TOKEN}`
};

var auxAuthorization = this.headers.Authorization.split(' ');

if (auxAuthorization[1] === '<INSERT_VALID_ACCESS_TOKEN>') {
    console.log('Set up a valid token');
    return;
}

setPublicAndPrivateKey();

const urlInicialization = serviceConfig.URL_INITIALIZE_SIGNATURE;
const urlFinalization = serviceConfig.URL_FINALIZE_SIGNATURE;

const initializeSignature = (URL) => {

    const formInicialization = {
        nonce: signatureConfig.NONCE,
        attached: signatureConfig.ATTACHED,
        profile: signatureConfig.PROFILE,
        hashAlgorithm: signatureConfig.HASH_ALGORITHM,
        certificate: getPublicKey(),
        operationType: signatureConfig.OPERATION_TYPE,
        'originalDocuments[0][nonce]': signatureConfig.NONCE_OF_ORIGINAL_DOCUMENT,
        'originalDocuments[0][content]': fs.createReadStream(signatureConfig.ORIGINAL_DOCUMENT_PATH),
    }

    return new Promise((resolve, reject) => {

        request.post({ url: URL, formData: formInicialization, headers: this.headers }, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })


}


const finalizeSignature = (URL_FINALIZATION, formFinalization) => {

    return new Promise((resolve, reject) => {

        request.post({ url: URL_FINALIZATION, formData: formFinalization, headers: this.headers }, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })

}

initializeSignature(urlInicialization).then(result => {
    console.log('Result of initialization: \n', result);

    var signedAttributes = result.signedAttributes

    var messageDigest;
    var messageDigestEncryptedAndBase64encoded;
    var signedAttributesArray;
    var nonceOfSignedAttributes;

    const formFinalization = {
        nonce: signatureConfig.NONCE,
        attached: signatureConfig.ATTACHED,
        profile: signatureConfig.PROFILE,
        hashAlgorithm: signatureConfig.HASH_ALGORITHM,
        certificate: getPublicKey(),
        operationType: signatureConfig.OPERATION_TYPE
    }

    for (var i = 0; i < signedAttributes.length; i++) {

        signedAttributesArray = signedAttributes[i];
        nonceOfSignedAttributes = signedAttributesArray.nonce;
        messageDigest = signedAttributesArray.content;

        var byteArray = Base64Binary.decode(messageDigest);

        // It will be necessary to encrypt with a private key (corresponding to the certificate informed in the initialization step of the signature) the value stored in the messageDigest variable and encode it in base64
        messageDigestEncryptedAndBase64encoded = cipher(Buffer.from(byteArray, 'binary').toString('hex'));

        formFinalization['finalizations[0][nonce]'] = nonceOfSignedAttributes;
        formFinalization['finalizations[0][signedAttributes]'] = messageDigest;
        formFinalization['finalizations[0][signatureValue]'] = messageDigestEncryptedAndBase64encoded;
        formFinalization['finalizations[0][document]'] = fs.createReadStream(signatureConfig.ORIGINAL_DOCUMENT_PATH);
    }

    finalizeSignature(urlFinalization, formFinalization).then(result => {
        console.log('\nResult of finalization:\n', result);

        var signaturesArray = result.signatures;

        var contentOfSignature = signaturesArray[0].content;

        var byteArray = Base64Binary.decode(contentOfSignature);

        fs.appendFile('CMSsignature.p7s', byteArray, function(err) {
            if (err) throw err;
            console.log('\nSignature created successful: assinatura-cms/CMSsignature.p7s');
        });

    }).catch(error => {
        console.log(error);
    })


}).catch(error => {
    console.log(error);
})