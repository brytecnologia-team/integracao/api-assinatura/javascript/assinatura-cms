var forge = require('node-forge');
var KJUR = require('jsrsasign')
var fs = require('fs');
const certificateConfig = require('../config/CertificateConfig');
const signatureConfig = require('../config/SignatureConfig');

var privateKey;
var publicKey;

function setPublicAndPrivateKey() {
    console.log('Setting up public and private key...\n');

    var p12Asn1 = forge.asn1.fromDer(
        fs.readFileSync(certificateConfig.PRIVATE_KEY_LOCATION, 'binary'));

    var pkcs12 = forge.pkcs12.pkcs12FromAsn1(p12Asn1, false, certificateConfig.PRIVATE_KEY_PASSWORD);

    //Then get the private key from pkcs12 of the desired certificate (see forge doc) and convert to PKCS # 8 to be imported with webcrypto
    // load keypair and cert chain from safe content(s) 
    for (var sci = 0; sci < pkcs12.safeContents.length; ++sci) {
        var safeContents = pkcs12.safeContents[sci];

        for (var sbi = 0; sbi < safeContents.safeBags.length; ++sbi) {
            var safeBag = safeContents.safeBags[sbi];

            if (safeBag.cert != undefined) {
                // DER-encode certificate and then base64-encode that
                var der = forge.asn1.toDer(forge.pki.certificateToAsn1(safeBag.cert));
                var b64 = forge.util.encode64(der.getBytes());

                this.publicKey = b64;
            }

            // this bag has a private key
            if (safeBag.type === forge.pki.oids.keyBag) {
                //Found plain private key
                privateKey = safeBag.key;
            } else if (safeBag.type === forge.pki.oids.pkcs8ShroudedKeyBag) {
                // found encrypted private key
                privateKey = safeBag.key;
            } else if (safeBag.type === forge.pki.oids.certBag) {
                // this bag has a certificate...        
            }
        }
    }
}

function getPublicKey() {
    return this.publicKey;
}

// Convert a hex string to a byte array
function hexToBytes(hex) {
    for (var bytes = [], c = 0; c < hex.length; c += 2)
        bytes.push(parseInt(hex.substr(c, 2), 16));
    return bytes;
}

const cipher = (messageDigest) => {

    var pki = forge.pki;

    // wrap an RSAPrivateKey ASN.1 object in a PKCS#8 ASN.1 PrivateKeyInfo
    var privateKeyInfo = pki.wrapRsaPrivateKey(pki.privateKeyToAsn1(privateKey));

    //console.log('Chave pkcs8 convertida', privateKeyInfo);

    // convert a PKCS#8 ASN.1 PrivateKeyInfo to PEM
    var pem = pki.privateKeyInfoToPem(privateKeyInfo)

    var hashAlgorithm = signatureConfig.HASH_ALGORITHM + 'withRSA';

    var sig = new KJUR.crypto.Signature({ "alg": hashAlgorithm });

    sig.init(pem);
    sig.updateHex(messageDigest);
    var signedData = sig.sign();

    var signedDataInBinary = hexToBytes(signedData);


    return Buffer.from(signedDataInBinary, 'binary').toString('base64');
}

module.exports = { cipher, getPublicKey, setPublicAndPrivateKey };