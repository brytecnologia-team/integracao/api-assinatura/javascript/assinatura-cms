module.exports = {

    URL_INITIALIZE_SIGNATURE: 'https://fw2.bry.com.br/api/cms-signature-service/v1/signatures/initialize',

    URL_FINALIZE_SIGNATURE: 'https://fw2.bry.com.br/api/cms-signature-service/v1/signatures/finalize',

    ACCESS_TOKEN: '<INSERT_VALID_ACCESS_TOKEN>'

}