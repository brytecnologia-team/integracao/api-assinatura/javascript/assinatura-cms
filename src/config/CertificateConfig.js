module.exports = {

    // If you don't have one, follow the instructions in README.md on how to obtain your personal private key.
    PRIVATE_KEY_LOCATION: '<LOCATION_OF_PRIVATE_KEY_FILE>',

    PRIVATE_KEY_PASSWORD: '<PRIVATE_KEY_PASSWORD>'
}