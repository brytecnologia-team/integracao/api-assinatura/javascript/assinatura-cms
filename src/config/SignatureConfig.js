module.exports = {

    //Request identifier
    NONCE: 1,

    // Available values: 'true' and 'false'.
    ATTACHED: 'true',

    // Available values: "BASIC", "CHAIN", "CHAIN_CRL", "TIMESTAMP", "COMPLETE", "ADRB", "ADRT", "ADRV", "ADRC", "ADRA", "ETSI_B", "ETSI_T", "ETSI_LT" and "ETSI_LTA".
    PROFILE: 'BASIC',

    // Available values: "SHA1", "SHA256" e "SHA512".
    HASH_ALGORITHM: 'SHA256',

    // Available values: "SIGNATURE", "CO_SIGNATURE	" and "COUNTER_SIGNATURE".
    OPERATION_TYPE: 'SIGNATURE',

    // Identifier of the original document within a batch
    NONCE_OF_ORIGINAL_DOCUMENT: 1,

    //location where the original document is stored    
    ORIGINAL_DOCUMENT_PATH: './arquivo-assinar/artigo.html'

}